%% 
%% Licence Modification Guidelines acoompanying FLA for KDE e.V.
%% Released under the the CC:ASA license.
%%
%% Language: English
%%

%{{{ Header

\documentclass[10pt]{article}
% \usepackage{german}%,umlaut}
\usepackage{fancyhdr}
\usepackage{alltt}
\usepackage{epsfig}
\usepackage[pdftex,pdfborder={0 0 0}]{hyperref}
\usepackage[english]{babel}
\usepackage[printonlyused]{acronym}
\IfFileExists{bera.sty}{\usepackage{bera}}{\typeout{No style file bera.}}
\usepackage{kde}

\selectlanguage{english}
\pagestyle{fancyplain}

\lhead[\fancyplain{}{\bfseries\thepage}]
        {\fancyplain{}{\bfseries\rightmark}}
\rhead[\fancyplain{}{\bfseries\leftmark}]
        {\fancyplain{}{\bfseries\thepage}}
\cfoot{}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\renewcommand{\thesection}{\S~\arabic{section}}

%
% alternative ways of doing it...
%

% this makes the references work correctly:
%\newcommand{\A}{\subsection{}}
%\renewcommand{\subsectionmark}[1]{\markright{\thesection\ }}

% this looks better:
\newcounter{absatz}[section]
\newcommand{\A}{\par\vspace{1ex}
                \stepcounter{absatz}\noindent(\arabic{absatz})~~}

% make the page a little wider
%\addtolength{\textwidth}{3cm}
%\addtolength{\headwidth}{3cm}
%\addtolength{\hoffset}{-1.5cm}
%\addtolength{\textheight}{2cm}
\addtolength{\textwidth}{3cm}
\addtolength{\headwidth}{3cm}
\addtolength{\hoffset}{-1.5cm}
\addtolength{\textheight}{2cm}

%}}}

\begin{document}

%{{{ Head of front page

\thispagestyle{empty}

\begin{center}
{\LARGE\bf \acl{FRP}}\\
(Version \FRPversion)
\end{center}

\begin{center}
\sc\small Copyright (C) 2007,2008 KDE e.V.,\\
\sc\small
\bgroup\let\\\relax\KDEEVaddress\egroup\\
\sc\small This licence is released under the terms of\\
the Creative Commons Attribution/Share-alike licence version 2.5.
\end{center}

%}}}

\begin{center}
{\Large\bf Preamble}
\end{center}
 
\input preamble.tex

KDE~e.V.~is given the right in the \acs{FLA} to relicense the 
software as necessary for the
long-term legal maintainability and protection of the software. 
Additional restrictions on relicensing are listed in the 
KDE \acf{FRP}.
This document contains the Relicensing Policy under which
such relicensing may occur.


\section{Scope and Modification}
\label{s.modification}

\begin{enumerate}

\item This is the \acf{FRP} version \FRPversion.
This \acs{FRP} applies to rights assigned
(or, in countries where that is not possible, licensed) to
KDE~e.V.~through the \acf{FLA} version \FLAversion.


\item This \acs{FRP} does not apply to later versions of the \acs{FLA}, nor
to any \acs{FLA} signed with other organizations than KDE~e.V.

\item \label{i.modification}
Modification of the terms of this \acs{FRP} is possible through a 
motion carried at the rules of the general assembly of KDE~e.V.~
under the same voting rules as a modification of the statutes
of KDE~e.V.~itself. Such a modification will 
increase the version number of this \acs{FRP} and apply
to all subsequent \acs{FLA} documents signed with KDE~e.V.
\end{enumerate}

\section{Definitions}

The licenses which are acceptable in KDE code may be roughly
categorized as ``weak'' and ``strong'' copyleft as well
as a category ``other.'' These licenses are:
\begin{itemize}
\item[Weak] \begin{itemize}
	\item The \acf{LGPL} version 2.1.\footnote{\url{http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html}}
	\item The \acf{LGPL} version 3.\footnote{\url{http://www.fsf.org/licensing/licenses/lgpl.html}}
	\item The \acs{LGPL} version 2.1 ``or later'' variant (see \acs{GPL} entry, below).
	\item The 2-clause \acs{BSD} license, with no advertising clause.\footnote{\url{http://opensource.org/licenses/bsd-license.php}}
	\item The X11R6 or MIT license.\footnote{\url{http://opensource.org/licenses/mit-license.html}}
	\end{itemize}

Code that is in the KDE modules kdelibs and kdepimlibs must be
licensed under a license that is one of these weak copyleft licenses.

\item[Strong] \begin{itemize}
	\item The \acf{GPL} version 2.\footnote{\url{http://www.gnu.org/licenses/old-licenses/gpl-2.0.html}}
	\item The \acs{GPL} version 2 with exceptions.\footnote{An additional clause is permitted in the license text: ``In addition, as a special exception, permission is granted to link to the Qt library version $[n]$ as distributed by Trolltech S.A.'' where $n$ is a version number for Qt.}
	\item The \acs{GPL} version 2 ``or later'' variant.\footnote{The phrase ``or, at your option, any later version of the GPL.'' may be added to the license text.}
	\item The \acs{GPL} version 3.\footnote{\url{http://www.fsf.org/licensing/licenses/gpl.html}}
	\item The \acs{GPL} version 3 with exceptions as noted for the \acs{GPL}.
	\item The \acs{GPL} version 3 ``or later'' variant.
	\item Either exception version with ``Trolltech S.A.'' replaced by ``Nokia Corporation.''
	\end{itemize}

These licenses are permitted in all KDE modules where a weak license is
not required.

\item[Other] \begin{itemize}
	\item The \acf{FDL} version 1.2 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
	\item The \acs{FDL} version 1.2 ``or later'' variant with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
	\end{itemize}

These licenses are additionally permitted in all KDE modules for
\emph{documentation only}.
\end{itemize}

These groups of acceptable licenses may be modified
by a vote of the general assembly of KDE~e.V.
A modification of the license groups is considered to
be a change to the \acs{FRP} and entails a new version number as 
described in item~\ref{s.modification}.\ref{i.modification}.

An indicative, but non-definitive, list of licenses may be found
on the KDE TechBase.\footnote{\url{http://techbase.kde.org/Policies/Licensing_Policy}}

\section{Form Requirements}

This section describes the textual requirements for licenses
within KDE. The \emph{form} in which a license is included in 
code has an effect not on the license itself but does affect
the ease of checking compliance with the license. Therefore
this \acs{FRP} describes specific forms which the license must take.
All code which falls under a \acs{FLA} must be accompanied by a full
license header in the source code itself stating the author(s)
and the license, as described in the FSFE code licensing guidelines.
% TODO URL for those guidelines

The author(s) of the code must be identified by full name and
an email address; if possible all authors with a
copyrightable contribution to the code should be identified.
A correspondence address --- the official KDE e.V. address ---
must be included as well.

The ``long form'' of the license text should be used in the header,
to avoid ambiuguity in the variations under a common name of a single license
(distinguishing the \acs{GPL} version 2 and \acs{GPL} version 3, for instance,
or the various \acs{BSD} licenses). Use the form of the license text included under the header ``how to apply these terms to your new programs'' (from the FSF website) for the GPL and LGPL licenses. Use the complete text of the 2-clause BSD license.

\section{Relicensing Policy}

In the event that KDE~e.V.~deems it necessary to relicense a portion of 
the source code for which it has an \acs{FLA}, the following procedure must be
used and the following restrictions obeyed.

\begin{enumerate}
\item KDE~e.V.~must make a reasonable effort to contact the author(s)
	of the source code involved who have signed the \acs{FLA}.
	A unanimous agreement among signing authors as to relicensing
	which is communicated to KDE~e.V.~is sufficient for any relicensing.

\item In the event that any author of the source code explicitly objects
	within a reasonable time frame (30 days)
	to the relicensing, KDE~e.V.~is prohibited from performing
	the proposed relicensing and must wait for 60 days before
	suggesting a new relicensing of the same source code.

\item The above clause applies to authors identified as living natural persons and
	legal entities (organizations) only; heirs and beneficiaries are excluded from
	the objection clause.

\item Relicensing changes that only change the license from one member of the ``weak''
	license group to another member of the same group, or one member of the
	``strong'' group to another member of that group, 
	are allowed after a reasonable attempt as described above.
	Relicensing within the ``other'' group does not fall under this
	clause (see clause~\ref{other-changes} below).

\item Relicensing changes that change licenses from one group
	to another must be posted on the kde-ev-membership mailing list
	and approved through an online vote.

\item Other relicensing proposals must be placed before the membership
	mailing list and approved through an online vote as described in
	the rules of procedure of KDE~e.V.
	\label{other-changes}%
	The results of such relicensing votes must be published
	on the website of KDE~e.V.
\end{enumerate}

\section{Miscellaneous}


Regarding the succession of rights in this contractual
relationship, German law shall apply, unless this Agreement imposes
deviating regulations. In case of the Beneficiary's death, the
assignment of exclusive rights shall continue with the heirs. In case
of more than one heir, all heirs have to exercise their rights through
a common authorized person.\\

Place of jurisdiction for all legal conflicts arising out of or in
connection with this Agreement is Munich, Germany.\\


\end{document}
